<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class UserFixture extends Fixture
{

  public function __construct(UserPasswordEncoderInterface $encoder)
  {
    $this->encoder = $encoder;
  }

  public function load(ObjectManager $manager)
  {
    $user = new User();
    $user->setEmail('enzo@test.com')
    ->setPassword($this->encoder->encodePassword($user, 'userpass'))
    ->setFirstname('Enzo')
    ->setLastName('Avagliano');

    $manager->persist($user);

    $admin = new User();
    $admin->setEmail('admin@test.com')
    ->setPassword($this->encoder->encodePassword($admin, 'adminpass'))
    ->setFirstname('Administrateur')
    ->setLastName('Hotel')
    ->setRoles(['ROLES_ADMIN']);

    $manager->persist($admin);

    $manager->flush();
  }
}
